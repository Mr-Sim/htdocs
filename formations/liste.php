<?php
require_once '../header.php';
require_once '../fonctions/formations.php';

//On récupère les informations de chaque formations dans la base de données
$listeFormations = getAllFormations();
?>

Liste des formations

<div>
    <form action=ajouter.php>
        <input type=submit name=ajouter value="Ajouter une formation">
    </form>

<div>
<table  class="table-bordered">
    <TR bgcolor="bbbbbb">
        <TH>ID</TH>
        <TH>Formation</TH>
        <TH>Etablissement concerné</TH>
        <TH>Module</TH>
    </TR>
        <?php //On insère chaque informations dans le tableau avec un foreach. 
        foreach($listeFormations as $item): 
            $nbmodule = 0;//Définit le nombre de modules dans une formation.

            //On récupère le nom de l'établissement associé à la formation.
            $etablissement = getEtablissementByFormation($item->idformation);
            foreach($etablissement as $param){
                $nomEtablissement = $param->Nom_etablissement;
            }

            //Pour chaque module, on récupère le nombre de séances associées, 
            //dans le seul but d'obtenir le nombre final de lignes occupées par une formation.
            $modules = getModulesByFormation($item->idformation);
            foreach($modules as $module){
                $nbmodule +=1;
            }?>

            <TR>
                <TD rowspan=<?php echo $nbmodule; ?>><?php echo $item->idformation;?></TD>
                <TD rowspan=<?php echo $nbmodule; ?>><?php echo $item->Nom_formation;?><br>
                    <p1><a href='modifier.php?edit=<?php echo $item->idformation;?>'>MODIFIER</a>
                    <a href='../model.php?supprimer_formation=<?php echo $item->idformation;?>' onClick="return(confirm('Etes-vous sûr de vouloir supprimer <?php echo $item->Nom_formation;?> ?'));">SUPPRIMER</a>
                </TD> 
                <TD rowspan=<?php echo $nbmodule; ?>><?php echo$nomEtablissement;?></TD>

            <?php //On récupère tous les modules d'une formation
            $modules = getModulesByFormation($item->idformation);
            foreach($modules as $module):?>
                <TD>
                <a href='module.php?module=<?php echo $module->idmodule;?>&formation=<?php echo $item->idformation; ?>'>
                    <?php echo $module->Nom_module;?>
                </a></TD>
                <TR> 
            <?php endforeach;?>
            
        <?php endforeach;?>
    </table>
</div>




<?php
require_once '../footer.php';
?>

