<?php
require_once '../header.php';
require_once('../fonctions/etablissements.php');
require_once('../fonctions/formations.php');
?>
Ajouter une formation

<div>
    <form action='../model.php' method=post>
    <input type=text name="nom" value="Nom de la formation"><br>

    Etablissement concerné : <br> <?php
        //On récupère l'id et le nom des établissements existants.
        $Etablissements = getAllEtablissements();
        foreach($Etablissements as $item):
            $idetablissement = $item->idetablissement;
            $nomEtablissement = $item->Nom_etablissement;?>

            <input type=radio name='etablissement' value='<?php echo $idetablissement; ?>'> <?php echo $nomEtablissement; ?><br>
        <?php endforeach;?>
        <input type=radio name='etablissement' value=0 >Indépendant/Autre<br>
    Modules de la formation : <br><?php
        //On récupère l'id et le nom des modules existants.
        $Modules = getAllModules(); 
        $nbmodule = 0; //On initialise le nombre total de modules disponible. Augmentera à chaque boucle.
        foreach($Modules as $item): 
            $nbmodule+=1; //On augmente le nombre de modules disponibles.
            $idModule = $item->idmodule;
            $nomModule = $item->Nom_module;?>
            
            <input type="checkbox" id="modules" name="module<?php echo $nbmodule;?>" value="<?php echo $idModule;?>">
                <label for="module<?php echo $nbmodule;?>"><?php echo $nomModule;?></label>
        <?php endforeach;?>
        <input type=hidden name="nbmodule" value="<?php echo $nbmodule;?>">
    <br><input type=submit name="ajouter_formation" value="Ajouter une formation"><br>
    </form>
</div>




<?php
require_once '../footer.php';
?>

