<?php
require_once('header.php');
?>
PAGE D'ACCUEIL
<?php
if(isset($_SESSION['nom'])&&!empty($_SESSION['nom'])){
    echo("connecté en tant que ".$_SESSION['nom']." ".$_SESSION['prenom'].".");
}
?>

<div class="container">
    <table class="table-bordered small">
        <thead>
            <tr>
                <th colspan="2">Les établissements</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><a href='etablissements/ajouter.php'>Ajouter un établissement</a></td>
                <td><a href='etablissements/liste.php'>Lister les établissements</a></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="container">
    <table class="table-bordered small">
        <thead>
            <tr>
                <th colspan="2">Les formations</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><a href='formations/ajouter.php'>Ajouter une formation</a></td>
                <td><a href='formations/liste.php'>Lister les formations</a></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="container">
    <table class="table-bordered small">
        <thead>
            <tr>
                <th colspan="2">Les intervenants</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><a href='intervenants/ajouter.php'>Ajouter un intervenant</a></td>
                <td><a href='intervenants/liste.php'>Lister les intervenants</a></td>
            </tr>
        </tbody>
    </table>
</div>


<?php
require_once('footer.php');
?>
