<?php
require_once "fonctions/operations.php";
require_once "fonctions/etablissements.php";
require_once "fonctions/intervenants.php";
require_once "fonctions/formations.php";
require_once "fonctions/factures.php";
require_once "fonctions/authentification.php";

##############################################################################
//ETABLISSEMENTS

if(isset($_POST['ajouter_etablissement'])){
    print_r($_POST);
    addEtablissement($_POST);
    header('location: etablissements/liste.php');
}
if(isset($_GET['supprimer_etablissement'])){
    print_r($_GET);
    deleteEtablissement($_GET);
    header('location: etablissements/liste.php');
}
if(isset($_POST['modifier_etablissement'])){
    print_r($_POST);
    editEtablissement($_POST);
    header('location: etablissements/liste.php');
}

##############################################################################
//INTERVENANTS

if(isset($_POST['ajouter_intervenant'])){
    print_r($_POST);
    addIntervenant($_POST);
    header('location: intervenants/liste.php');
}
if(isset($_GET['supprimer_intervenant'])){
    print_r($_GET);
    deleteIntervenant($_GET);
    header('location: intervenants/liste.php');
}
if(isset($_POST['modifier_intervenant'])){
    print_r($_POST);
    editIntervenant($_POST);
    header('location: intervenants/liste.php');
}

##############################################################################
//FORMATIONS

if(isset($_POST['ajouter_formation'])){
    print_r($_POST);
    addFormation($_POST);
    header('location: formations/liste.php');
}
if(isset($_GET['supprimer_formation'])){
    print_r($_GET);
    deleteFormation($_GET);
    header('location: formations/liste.php');
}
if(isset($_POST['modifier_formation'])){
    print_r($_POST);
    editFormation($_POST);
    header('location: formations/liste.php');
}

##############################################################################
//FACTURES

if(isset($_POST['ajouter_facture'])){
    print_r($_POST);
    addFacture($_POST);
    header('location: factures/liste.php');
}
if(isset($_GET['generer_facture'])){
    print_r($_GET);
    generateFacture($_GET);
    header('location: factures/liste.php');
}
if(isset($_POST['modifier_facture'])){
    print_r($_POST);
    editFacture($_POST);
    header('location: factures/liste.php');
}

##############################################################################
//Utilisateurs

if(isset($_POST['ajouter_utilisateur'])){
    print_r($_POST);
    addUtilisateur($_POST);
    header('location: authentification/liste.php');
}
if(isset($_GET['supprimer_utilisateur'])){
    print_r($_GET);
    deleteUtilisateur($_GET);
    header('location: authentification/liste.php');
}
if(isset($_POST['modifier_utilisateur'])){
    print_r($_POST);
    editUtilisateur($_POST);
    header('location: authentification/liste.php');
}

if(isset($_POST['connexion'])){
    print_r($_POST);
    $connect = getAuthentification($_POST);
    if($connect){
        session_start();
        $_SESSION['nom']=$connect->Nom_utilisateur;
        $_SESSION['prenom']=$connect->Prenom_utilisateur;
        header('location: ../index.php');
    }
    else{
        header('location: 404');
    }
}

##############################################################################






?>