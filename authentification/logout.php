<?php
//On récupère la session
session_start();
//On détruit les variables de notre session
session_unset();
//On detruit la session
session_destroy();
//On redirige l'utilisateur vers la page d'accueil
header('location: ../index.php');