
<?php
require_once '../header.php';
require_once('../fonctions/intervenants.php');
$listeDiplomes = getAllDiplomes();
$listeSpecialites = getAllSpecialites();
?>


Ajouter un intervenant

<div>
    <form action='../model.php' method=post>
        <input type=text name="nom" value="Nom de l intervenant"><br>
        <input type=text name="adresse" value="Adresse"><br>
        <input type=text name="email" value="Email"><br>
        
        <p>Date de naissance :</p>
        <ul id="date">
            <li>
                <select name="jour" id="list">
                    <option selected>Jour</option>
                    <?php
                    for($i=1 ; $i<=31 ; $i++){
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }?>
                </select>

                <select name="mois" id="list">
                    <option selected>Mois</option>
                    <?php
                    for($i=1 ; $i<=12 ; $i++){
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }?>
                </select>

                <select name="annee" id="list">
                    <option selected>Année</option>
                    <?php
                    $anneeActuelle = date("Y");
                    for($i=$anneeActuelle ; $i>=$anneeActuelle-100 ; $i--){
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }?>
                </select>

        <input type=submit name="ajouter_intervenant" value="Ajouter un intervenant"><br>
    </form>
</div>




<?php
require_once '../footer.php';
?>

