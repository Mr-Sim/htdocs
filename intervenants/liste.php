<?php
require_once '../header.php';
require_once '../fonctions/intervenants.php';
require_once('../fonctions/operations.php');

//On récupère les informations de chaque établissements dans la base de données
$listeIntervenants = getAllIntervenants();

?>

Liste des intervenants

<div>
    <form action=ajouter.php>
        <input type=submit name=ajouter value="Ajouter un intervenant">
    </form>

<div>
    <table class="table-bordered">
        
        <th>ID</th>
        <th>Intervenant</th>
        <th>Adresse</th>
        <th>Email</th>
        <th>Date de naissance</th>
        <th>Diplômes</th>
        <th>Spécialisation</th>
    
        <?php //On insère chaque informations dans le tableau avec un foreach. 
        foreach($listeIntervenants as $item): ?>
        <tr>
            <td><?php echo $item->idintervenant;?></td>
            <td><?php echo $item->Nom_intervenant;?><br>
                <p1><a href='modifier.php?edit=<?php echo $item->idintervenant;?>'>MODIFIER</a>
                <a href='../model.php?supprimer_intervenant=<?php echo $item->idintervenant;?>' 
                onClick="return(confirm('Etes-vous sûr de vouloir supprimer <?php echo $item->Nom_intervenant;?> ?'));">SUPPRIMER</a></td>
           
            <td><?php echo $item->Adresse_intervenant;?></td>
            <td><?php echo $item->Email_intervenant;?></td>

            <td><?php echo inverserDate($item->Date_naissance_intervenant); ?></td>

            <td><?php //On récupère la liste de tous les diplômes possédés par l'intervenant
                $listeDiplomes = getDiplomesByIntervenant($item->idintervenant);
                //On insère le nom de chaque diplômes dans la case du tableau
                foreach($listeDiplomes as $diplome):
                    echo $diplome->Nom_diplome;
                    echo"<br>";
                endforeach;
            ?></td>
            
            <td><?php //On récupère la liste de toutes les spécialités de l'intervenant
                $listeSpecialite = getSpecialitesByIntervenant($item->idintervenant);
                //On insère le nom de chaque spécialité dans la case du tableau
                foreach($listeSpecialite as $specialite):
                    echo $specialite->Nom_specialite;
                    echo"<br>";
                endforeach;
            ?></td>
            
        </tr>
        <?php endforeach; ?>
    </table>
</div>




<?php
require_once '../footer.php';
?>

