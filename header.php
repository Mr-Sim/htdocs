
<?php session_start();?>
<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link href="/css/style.css" rel="stylesheet" type="text/css">
 
    <title>ENSLAR</title>
</head>

<body class="d-flex flex-column h-100">
<header>
<section class="banner-area" id="home">
    <div id="banner"></div>
</section>



<nav class="navbar navbar-expand-md navbar-dark bg-dark">
    <a class="navbar-brand" href="/index.php"><img class="picture" src="/img/enslar.png"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
    aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/index.php">PAGE D'ACCUEIL</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-haspopup="true"aria-expanded="false">Etablissements</a>
                <ul class="dropdown-menu" aria-labelledby="dropdown1">
                    <li><a class="dropdown-item" href="/etablissements/liste.php">Liste des établissements</a></li>
                    <li><a class="dropdown-item" href="/etablissements/ajouter.php">Ajouter un établissement</a></li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-haspopup="true"aria-expanded="false">Intervenants</a>
                <ul class="dropdown-menu" aria-labelledby="dropdown1">
                    <li><a class="dropdown-item" href="/intervenants/liste.php">Liste des intervenants</a></li>
                    <li><a class="dropdown-item" href="/intervenants/ajouter.php">Ajouter un intervenant</a></li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-haspopup="true"aria-expanded="false">Formations</a>
                <ul class="dropdown-menu" aria-labelledby="dropdown1">
                    <li><a class="dropdown-item" href="/formations/liste.php">Liste des formations</a></li>
                    <li><a class="dropdown-item" href="/formations/ajouter.php">Ajouter une formation</a></li>
                </ul>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-haspopup="true"aria-expanded="false">Facturation</a>
                <ul class="dropdown-menu" aria-labelledby="dropdown1">
                    <li><a class="dropdown-item" href="/factures/liste.php">Liste des factures</a></li>
                    <li><a class="dropdown-item" href="/factures/ajouter.php">Ajouter une facture</a></li>
                </ul>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <?php if (isset($_SESSION['nom'])):?>
                    <a class="nav-link" href="authentification/logout.php">Déconnexion</a>
                <?php else: ?>
                    <a class="nav-link" href="authentification/login.php">Connexion</a>
                <?php endif; ?>
            </li>
            <li class="nav-item">
                <?php if (isset($_SESSION['role']) && $_SESSION['role']=="admin"): ?>
                    <a class="nav-link" href="users.php">Espace admin</a>
                <?php endif; ?>
            </li>
        </ul>
    </div>
</nav>
</header>


