<?php
require_once '../header.php';
require_once('../fonctions/factures.php');
require_once('../fonctions/operations.php');
require_once('../fonctions/etablissements.php');

//On récupère l'id de la facture dans l'URL
$id=$_GET['edit'];
print $id;
//On répupère les informations de la facture à partir de son id.
$facture = getFacture($id);
print_r($facture);
//On affecte chaque information à une variable.


foreach ($facture as $param):
    $numeroFacture = $param->Numero_facture;
    $dateFacture = $param->Date_facture;
    $dateFacture = inverserDate($dateFacture);
    $montant = $param->Montant_facture;
    $phase = $param->Phase;
    $etablissement = $param->idetablissement;   
endforeach;
?>

Modifier une facture :

<div>
    <form action='../model.php' method=post>
    ID Facture : <input readonly type=text name="id" value="<?php echo $id; ?>"><br>
    Numéro de facture : <input readonly type=text name="numeroFacture" value="<?php echo $numeroFacture; ?>"><br>
    Date : <input type=text name="date" value="<?php echo $dateFacture; ?>"><br>
    <!-- Montant : <input type=text name="montant" value="<?php #echo $montant; ?>"><br> -->
    Phase :
        <input type=radio name=phase value="Envoyée" <?php if($phase=="Envoyée"){echo "checked";} ?>>Envoyée
        <input type=radio name=phase value="Réglée" <?php if($phase=="Réglée"){echo "checked";} ?>>Reglée
        <input type=radio name=phase value="En attente" <?php if($phase=="En attente"){echo "checked";} ?>>En attente <br>

    Etablissement facturé :
    <select name="etablissement">
    <?php
    $listeEtablissements = getAllEtablissements();
    foreach($listeEtablissements as $item): 
        $idetablissement = $item->idetablissement;
        $nomEtablissement = $item->Nom_etablissement;?>
        <option value='<?php echo $idetablissement;?>' <?php if($idetablissement==$etablissement){echo"selected";}?>>
            <?php echo $nomEtablissement;?>
        </option>
    <?php endforeach;?>
    </select><br>
    <input type=submit name="modifier_facture" value="Enregistrer les modifications"><br>
    </form>
</div>




<?php
require_once '../footer.php';
?>
