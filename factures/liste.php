<?php
require_once('../header.php');
require_once('../fonctions/factures.php');
require_once('../fonctions/operations.php');
require_once('../fonctions/etablissements.php');

//On récupère les informations de chaque établissements dans la base de données
$listeFactures = getAllFactures();

?>

Liste des factures

<div>
    <form action=ajouter.php>
        <input type=submit name=ajouter value="Ajouter une facture">
    </form>

<div>
    <table class="table-bordered">
        
        <th>ID</th>
        <th>Numéro de facture</th>
        <th>Date</th>
        <th>Montant</th>
        <th>Phase</th>
        <th>Etablissement facturé</th>
    
        <?php //On insère chaque informations dans le tableau avec un foreach. 
        foreach($listeFactures as $item): ?>
        <tr>
            <td><?php echo $item->idfacture;?></td>
            <td><?php echo $item->Numero_facture;?><br>
                <p1><a href='modifier.php?edit=<?php echo $item->idfacture;?>'>MODIFIER</a>
                <a href='generer.php?generer_facture=<?php echo $item->idfacture;?>' 
                onClick="return(confirm('Etes-vous sûr de vouloir générer la facture <?php echo $item->Numero_facture;?> ?'));">GÉNÉRER</a></td>
           
            <td><?php echo inverserDate($item->Date_facture);?></td>
            <td><?php echo $item->Montant_facture."€";?></td>
            <td><?php echo $item->Phase; ?></td>
            

            <?php $etablissement = getEtablissement($item->idetablissement);
            foreach($etablissement as $param){
                $nomEtablissement = $param->Nom_etablissement;
            }?>
            <td><?php echo $nomEtablissement;?></td>           
        </tr>
        <?php endforeach; ?>
    </table>
</div>




<?php
require_once '../footer.php';
?>

