<?php
require_once '../fpdf/fpdf.php';
require_once('../fonctions/etablissements.php');
require_once('../fonctions/intervenants.php');
require_once('../fonctions/formations.php');
require_once('../fonctions/factures.php');
require_once('../fonctions/operations.php');

//Onb récupère l'id de la facture. Si il n'est pas définit, on retourne l'utilisateur à la liste des factures.
if(isset($_GET['generer_facture'])){
    $idfacture=$_GET['generer_facture'];
}
else{
    header('location : liste.php');
}

class PDF extends FPDF{

    // Header de la facture
    //Mettre $str = utf8_decode($str) dans les cellules pour faire fonctionner les caractères spéciaux. 
    //Caractère € incompatible.
    function Header(){

        //Definition des variables
        $idfacture=$_GET['generer_facture'];
        $facture = getFacture($idfacture);
        foreach($facture as $info){
            $numeroFacture = $info->Numero_facture;
            $montant = $info->Montant_facture;
            $dateFacture = $info->Date_facture;
            $idetablissement = $info->idetablissement;
        }

        //On récupère l'id de l'établissement facturé. Cet ID servira aux requêtes SQL
        $etablissement = getEtablissement($idetablissement);
        foreach($etablissement as $info){
            $nomEtablissement = $info->Nom_etablissement;
            $adresseEtablissement = $info->Adresse_etablissement;
        }
        
        //Placement de l'image et de la ligne horizontale en haut de la facture.
        $this->Image('../img/enslar.png',90,3,30,);
        $this->SetLineWidth(1.5);
        $this->Line(10, 17, 210-10, 17);

        // Couleurs, épaisseur du trait et police grasse
        $this->SetFillColor(255,0,0);
        $this->SetTextColor(0);
        $this->SetDrawColor(255,0,0);
        $this->SetLineWidth(.3);

        // Identité de l'expediteur et du destinataire
        $this->SetY(20);
        $this->SetFont('Arial','BI',16);
        $this->Cell(100,8,'LAROUSSI Reda',2);
        $this->Cell(100,8,utf8_decode($nomEtablissement),2);
        $this->Ln();
        
        // Autres données de l'expediteur et du destinataire
        $this->SetFont('Arial','',12);
        $this->Cell(100,5,'SIREN 812500999',2);
        $this->Cell(100,5,utf8_decode($adresseEtablissement),2);
        $this->Ln();
        $this->Cell(100,5,utf8_decode('53 rue de Liège'),2);
        $this->Cell(100,5,'Localisation Etablissement',2); //Numéro departementale et ville de l'etablissement. Valeur absente dans la base de données.
        $this->Ln();
        $this->Cell(100,5,'77550 Moissy Cramayel',2);
        $this->Cell(100,5,'',2);
        $this->Ln();
        $this->Cell(100,5,utf8_decode('Tél.:06 67 14 14 31'),2);
        $this->Cell(100,5,'',2);
        $this->Ln();
        $this->Cell(100,5,'reda.laroussi.pro@gmail.com',2);
        $this->Cell(100,5,'',2);
        $this->Ln();
        
        $this->SetY(60);

        //Numéro de facture et date
        $this->Cell(100,5,utf8_decode("Facture n° : $numeroFacture"),2);
        $dateFacture = inverserDate($dateFacture);
        $this->Cell(100,5,utf8_decode("Date : $dateFacture"),2);
        $this->Ln();

        $this->SetY($this->GetY()+10);      
    }

    //Footer de la facture
    function Footer(){

        $this->SetX(10);
        $this->SetFont('','',9);
        $this->Cell(110,8,utf8_decode('Date de règlement : dès réception'),2);
        $this->Ln(3);
        $this->Cell(110,8,utf8_decode("Pénalité de retard : 3 fois le taux de l'intéret légal annuel actuel"),2);
        $this->Ln(3);
        $this->Cell(110,8,utf8_decode('RIB'),2);
        $this->Ln(6);

        $this->SetFont('','BI',9);
        $this->Cell(110,8,utf8_decode('LAROUSSI Reda - SIRET 81250099900014'),2);
        $this->Ln(3);
        $this->SetFont('','',9);
        $this->Cell(110,8,utf8_decode('53 rue de Liège, 77550 Moissy Cramayel'),2);
        $this->Ln(3);
        $this->Cell(110,8,utf8_decode('06 67 14 14 31 : reda.laroussi.pro@gmail.com'),2);
        $this->Ln(3);
        $y = $this->GetY();
        $this->SetLineWidth(.3);
        $this->Line(10, $y+6, 210-10, $y+6);
        $this->SetY($this->GetY()+5);
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }

    function Facture($idfacture){

        //Redéfinition des variables
        $idfacture=$_GET['generer_facture'];
        $facture = getFacture($idfacture);
        foreach($facture as $info){
            $idetablissement = $info->idetablissement;
        }
       
        $this->SetFillColor(20,0,0);
        $this->SetTextColor(0);
        $this->SetDrawColor(0,0,0);
        $this->SetLineWidth(.3);

        /** APPERCU GENERAL */{    
            /** HAUT DU TABLEAU */{
                $this->SetFont('Arial','I',10);
                $x=$this->GetX();
                $y=$this->GetY();
                $this->MultiCell(75,10,utf8_decode('Désignation des prestations'),1,'C');
                $this->SetXY($x+75,$y);

                $x=$this->GetX();
                $y=$this->GetY();
                $this->MultiCell(20,5,utf8_decode("Quantité \n (Heure)"),1,'C');
                $this->SetXY($x+20,$y);

                $x=$this->GetX();
                $y=$this->GetY();
                $this->MultiCell(45,5,utf8_decode("Prix unitaire \n HT (Euro)"),1,'C');
                $this->SetXY($x+45,$y);

                $this->MultiCell(50,5,utf8_decode("Total HT \n (Euro)"),1,'C');
            }
            
            //On récupère la liste des modules
            $modules = getModulesByFormationByEtablissement($idetablissement);

            $idtype_seance=0;
            $total = 0;
            foreach($modules as $module){
                //On récupère le nom et l'id du module
                $nomModule = $module->Nom_module;
                $idmodule = $module->idmodule;
                //On récupère toutes les séances de ce module
                $seances = getSeancesByModule($idmodule);
                //On teste tous les id de type de séances. 
                for($idType=0 ; $idType<6 ; $idType++){
                    $heures = 0;
                    $prixHT = 0;
                    foreach($seances as $seance){
                        
                        $totalHT = 0;
                        
                        $idtype_seance = $seance->idtype_seance;
                        $idseance = $seance->idseance;

                        //Pour chaque séances correspondant à l'id de type testé, on ajoute son nombre d'heures au total.
                        if($idType == $idtype_seance){
                            $h = getHeuresBySeance($idseance);
                            $h = $h->Duree_seance;
                            $heures+=$h;
                            $typeSeance = getTypeSeance($idtype_seance);
                            foreach($typeSeance as $info){
                                $nomTypeSeance = $info->Nom_type_seance;
                                $prixHT = $info->Taux_horaire;
                            }
                        } 
                    }
                    //Si un type de séances testé possède plus de 0 heures, on l'ajoute au tableau avec le nom du module et les autres valeurs récupérées.
                    if($heures>0){
                        
                        $totalHT = $prixHT*$heures;
                        $total += $totalHT;

                        $this->SetFont('Arial','B',12);
                        $x=$this->GetX();
                        $y=$this->GetY();
                        $this->MultiCell(75,10,utf8_decode("$nomTypeSeance $nomModule"),1,'C');
                        $this->SetXY($x+75,$y);

                        $x=$this->GetX();
                        $y=$this->GetY();
                        $this->MultiCell(20,10,utf8_decode("$heures"),1,'C');
                        $this->SetXY($x+20,$y);

                        $x=$this->GetX();
                        $y=$this->GetY();
                        $this->MultiCell(45,10,utf8_decode("$prixHT"),1,'C');
                        $this->SetXY($x+45,$y);

                        $this->MultiCell(50,10,utf8_decode("$totalHT"),1,'C');

                    }
                    
                }
            }

            $this->SetFont('Arial','B',12);
            $this->SetFillColor(200, 200, 210);
            $x=$this->GetX();
            $y=$this->GetY();
            $this->MultiCell(140,10,utf8_decode("Total"),1,'C', TRUE);
            $this->SetXY($x+140,$y);

            $x=$this->GetX();
            $y=$this->GetY();
            $this->MultiCell(50,10,utf8_decode("$total"),1,'C', TRUE);
            $this->SetFont('Arial','I',10);
            $this->MultiCell(190,12,utf8_decode("TVA non applicable, art.293 B du CGI"),1,'R');
        }
        /** TABLEAU EN DETAILS */{    
            /** HAUT DU TABLEAU */{
                $this->SetFont('Arial','B',12);
                $this->MultiCell(190,7,utf8_decode('Détails des prestations'),1,'C');
                
                $x=$this->GetX();
                $y=$this->GetY();
                $this->MultiCell(75,10,utf8_decode("Date"),1,'C');
                $this->SetXY($x+75,$y);

                $x=$this->GetX();
                $y=$this->GetY();
                $this->MultiCell(20,5,utf8_decode("Nombre \n heures"),1,'C');
                $this->SetXY($x+20,$y);

                $x=$this->GetX();
                $y=$this->GetY();
                $this->MultiCell(45,10,utf8_decode("Type de cours"),1,'C');
                $this->SetXY($x+45,$y);

                $this->MultiCell(50,10,utf8_decode("Classe"),1,'C');
            }
            $this->SetFont('Arial','',12);
            //Cette variable correspond aux coordonnées X où peuvent être placées les cellules à droite de la cellule date.
            $xdate = $this->GetX()+75;
            //Récupère les dates contenant des séances.
            $dates = getDatesBySeancesByEtablissement($idetablissement);
            
            foreach($dates as $item){
                $y=$this->GetY();
                //Saut de page automatique si le tableau descend trop bas.
                if($y > 250){ //plus la valeur est grande, plus le tableau peut descendre bas.
                    $this->AddPage();
                    $this->SetY(75); //Plus la valeur est petite, plus le tableau s'affiche en haut.
                }
                $date = $item->Date_seance;
                
                //Pour chaque date, on récupère les séances à l'interieur.
                $seances = getSeancesByEtablissementByDate($idetablissement,$date);
                $nbseance = 0; //Le nombre de séances sert à determiner la taille de la cellule date
                foreach($seances as $seance){
                    $nbseance+=1;
                    $heures = $seance->Duree_seance;
                    $classe = $seance->Nom_classe;
                    $typeSeance = $seance->Nom_type_seance;
                    $this->SetX($xdate);

                    $x=$this->GetX();
                    $y=$this->GetY();

                    $this->MultiCell(20,5,utf8_decode($heures),1,'C');
                    $this->SetXY($x+20,$y);

                    $x=$this->GetX();
                    
                    $this->MultiCell(45,5,utf8_decode($typeSeance),1,'C');
                    $this->SetXY($x+45,$y);

                    
                    $this->MultiCell(50,5,utf8_decode($classe),1,'C');
                    

                }
                //Une fois les séances affichées, on place la cellule de date avec la bonne taille.
                $ydate = $this->GetY()-$nbseance*5;
                $this->SetY($ydate);
                $this->MultiCell(75,$nbseance*5,utf8_decode(inverserDate($date)),1,'C');
                
            }
            //Cellule de remplissage vide, remplissant le document jusqu'au bas de page à la fin du tableau.
            $y=$this->GetY();
            $reste = 255-$y;
            $this->Multicell(190,$reste,'',1);
        }
        
    }
}

//Génération du PDF.
$pdf = new PDF( 'P', 'mm', 'A4' ); //A4, portrait, mesurement en mm;
$pdf->AliasNbPages();
//$pdf->SetAutoPageBreak(true,50);  # Problèmes avec l'autobreak. Choix d'un saut de page procédural (voir ligne 254)
$pdf->SetFont('Arial','',14);
$pdf->AddPage();
$pdf->Facture($idfacture);
$pdf->Output();
?>