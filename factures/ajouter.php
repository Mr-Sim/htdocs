
<?php
require_once '../header.php';
require_once('../fonctions/factures.php');
require_once('../fonctions/etablissements.php');
?>

<?php
//On récupère les séances traitées par la facture
?>


Ajouter une facture

<div>
    <form action='../model.php?ajouter_facture' method=post>
        <p>Date de facturation :</p>
        <ul id="date">
            <li>
                <select required name="jour">
                    <option disabled value='' selected>Jour</option>
                    <?php
                    for($i=1 ; $i<=31 ; $i++){
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }?>
                </select>

                <select required name="mois">
                    <option disabled value='' selected id="to_hide">Mois</option>
                    <?php
                    for($i=1 ; $i<=12 ; $i++){
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }?>
                </select>

                <select required name="annee">
                    <option disabled value='' selected>Année</option>
                    <?php
                    $anneeActuelle = date("Y");
                    for($i=$anneeActuelle ; $i>=$anneeActuelle-100 ; $i--){
                        echo '<option value="'.$i.'">'.$i.'</option>';
                    }?>
                </select>
            </li>
        </ul>
    
        <select required name="idetablissement">
            <option disabled value='' selected>Etablissement facturé</option>
            <?php
            $etablissements = getAllEtablissements();?>
            <?php foreach($etablissements as $item):
                $idetablissement = $item->idetablissement;
                $nomEtablissement = $item->Nom_etablissement;?>
                <option value='<?php echo $idetablissement;?>'> <?php echo $nomEtablissement;?></option>
            <?php endforeach;?>
        </select><br><br>

        <!--<input type=text name='montant' value='Montant de la facture' onClick="value=''"><br>-->

        Phase de facturation :<br>
        <input type=radio name='phase' value='Envoyée'>Envoyée<br>
        <input type=radio name='phase' value='Réglée'>Réglée<br>
        <input type=radio name='phase' value='En attente' checked>En attente<br>
            
        <input type=submit name="ajouter_facture" value="Ajouter une facture"><br>
    </form>
</div>




<?php
require_once '../footer.php';
?>

