<?php
require_once('connect-db.php');

/**Authentification*/{
   
    /**
     * Authentifier un utilisateur.
     * Recherche dans la base de données un utilisateur ayant un login et un mot de passe qui correspondent aux paramètres?
     * @param Login_utilisateur,Password_utilisateur
     * @return Tableau,false
     */
    function getAuthentification($params){
     
        global $pdo;

        $login=$params['login'];
        $password= $params['password'];

        $query="SELECT DISTINCT * FROM utilisateur WHERE Login_utilisateur='$login' AND Password_utilisateur='$password';";
        $prep=$pdo->prepare($query);
        $prep->execute();
        //On vérifie que la requête ne retourne qu'une ligne
        if($prep->rowCount() == 1){
            $result=$prep->fetch();
            return $result;
        }
        else{
            return false;
        }
    }
   
    /**
     * Récupère les informations de chaque utilisateurs.
     * @return Tableau
     */
        
    function getAllUsers(){

        global $pdo;

        $query = "SELECT * FROM utilisateur";
        try{
            $result = $pdo->query($query)->fetchAll();
            return $result;
        }
        catch (Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère les informations d'un utilisateur donné.
     * @param idutilisateur
     * @return Tableau
     */
    function getUtilisateur($id){

        global $pdo;

        $query = 'SELECT * FROM utilisateur WHERE idutilisateur = :id ;';
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Ajoute un utilisateur à la base de données.
     * @param Tableau $_POST du formulaire utilisateur/ajouter.php
     */
    function addUtilisateur($params){

        global $pdo;

        $nom = $params['nom'];
        $prenom = $params['prenom'];
        $login = $params['login'];
        $password = $params['password'];
        $telephone = $params['telephone'];
        $email = $params['email'];

        $query = "INSERT INTO utilisateur VALUES ('', '$nom', '$prenom', '$login', '$password', '$telephone', '$email');";
        try{
            $pdo->query($query);
        }
        catch(Exeption $e){
            die("erreur dans la requête ".$e->getMessage());
        }
    }

    /**
     * Supprime un utilisateur de la base de données.
     * @param idutilisateur
     */
    function deleteUtilisateur($params){
        
        global $pdo;
        
        $id = $params['supprimer_utilisateur'];
        
        $query = "DELETE FROM utilisateur WHERE utilisateur.idutilisateur = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
        }
        catch(Exeption $e){
            die("ERREUR : ".$e->getMessage());
        }
    }

    /**
     * Edite un utilisateur de la base de données. 
     * @param Tableau $_POST du formulaire
     */
    function editUtilisateur($params){

        global $pdo;

        $id = $params['id'];
        $nom = $params['nom'];
        $prenom = $params['prenom'];
        $login = $params['login'];
        $password = $params['password'];
        $telephone = $params['telephone'];
        $email = $params['email'];

        $query = "UPDATE utilisateur SET 
        Nom_utilisateur='$nom', 
        Prenom_utilisateur='$type', 
        Login_utilisateur='$adresse', 
        Password_utilisateur='$site_web', 
        Telephone_utilisateur='$telephone', 
        Email_utilisateur='$email'
        WHERE idutilisateur = $id;";
        try{
            $pdo->query($query);
        }
        catch(Exeption $e){
            die("erreur dans la requête ".$e->getMessage());
        }
    }
}