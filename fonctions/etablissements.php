<?php
require_once('connect-db.php');

/**Etablissements*/{
    /**
     * Récupère les infotmations de chaque établissements.
     * @return Tableau
     */
        
    function getAllEtablissements(){

        global $pdo;

        $query = "SELECT * FROM etablissement";
        try{
            $result = $pdo->query($query)->fetchAll();
            return $result;
        }
        catch (Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère les informations d'un établissement donné.
     * @param idetablissement
     * @return Tableau
     */
    function getEtablissement($id){

        global $pdo;

        $query = 'SELECT * FROM etablissement WHERE idetablissement = :id ;';
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Ajoute un établissement à la base de données.
     * @param Tableau $_POST du formulaire etablissement/ajouter.php
     */
    function addEtablissement($params){

        global $pdo;

        $nom = $params['nom'];
        $adresse = $params['adresse'];
        $type = $params['type'];
        $site_web = $params['site_web'];
        $telephone = $params['telephone'];
        $email = $params['email'];
        $interlocuteur = $params['interlocuteur'];

        $query = "INSERT INTO etablissement VALUES ('', '$nom', '$type', '$adresse', '$site_web', '$telephone', '$email', '$interlocuteur');";
        try{
            $pdo->query($query);
        }
        catch(Exeption $e){
            die("erreur dans la requête ".$e->getMessage());
        }
    }

    /**
     * Supprime un établissement de la base de données.
     * @param idetablissement
     */
    function deleteEtablissement($params){
        
        global $pdo;
        
        $id = $params['supprimer_etablissement'];
        
        $query = "DELETE FROM etablissement WHERE etablissement.idetablissement = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
        }
        catch(Exeption $e){
            die("ERREUR : ".$e->getMessage());
        }
    }

    /**
     * Edite un établissement de la base de données.
     * @param Tableau $_POST du formulaire
     */
    function editEtablissement($params){

        global $pdo;

        $id = $params['id'];
        $nom = $params['nom'];
        $adresse = $params['adresse'];
        $type = $params['type'];
        $site_web = $params['site_web'];
        $telephone = $params['telephone'];
        $email = $params['email'];
        $interlocuteur = $params['interlocuteur'];

        $query = "UPDATE etablissement SET 
        Nom_etablissement='$nom', 
        Type_etablissement='$type', 
        Adresse_etablissement='$adresse', 
        Site_web_etablissement='$site_web', 
        Telephone_etablissement='$telephone', 
        Email_etablissement='$email', 
        Interlocuteur_etablissement='$interlocuteur'
        WHERE idetablissement = $id;";
        try{
            $pdo->query($query);
        }
        catch(Exeption $e){
            die("erreur dans la requête ".$e->getMessage());
        }
    }
}

