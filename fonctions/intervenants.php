<?php
require_once('connect-db.php');

/**Intervenants*/{

    /**
     * Récupère les infotmations de chaque intervenants.
     * @return Tableau
     */  
    function getAllIntervenants(){

        global $pdo;

        $query = "SELECT * FROM intervenant;";
        try{
            $result = $pdo->query($query)->fetchAll();
            return $result;
        }
        catch (Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    /**
     * récupère tous les diplômes dans la base de données.
     * @return Tableau
     */
    function getAllDiplomes(){

        global $pdo;

        $query = "SELECT * FROM diplome;";
        try{
            $result = $pdo->query($query)->fetchAll();
            return $result;
        }
        catch (Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    
    /**
     * récupère toutes les spécialités dans la base de données.
     * @return Tableau
     */
    function getAllSpecialites(){

        global $pdo;

        $query = "SELECT * FROM specialite;";
        try{
            $result = $pdo->query($query)->fetchAll();
            return $result;
        }
        catch (Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère le nom des diplômes possédés par un intervenant.
     * @param idintervenant
     * @return Tableau
     */
    function getDiplomesByIntervenant($id){

        global $pdo;

        $query = 
        "SELECT diplome.Nom_diplome FROM diplome, intervenant_has_diplome
        WHERE diplome.iddiplome = intervenant_has_diplome.iddiplome 
        AND intervenant_has_diplome.idintervenant = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère le nom des spécialités d'un intervenant.
     * @param idintervenant
     * @return Tableau
     */
    function getSpecialitesByIntervenant($id){

        global $pdo;

        $query = 
        "SELECT specialite.Nom_specialite FROM specialite, intervenant_has_specialite
        WHERE specialite.idspecialite = intervenant_has_specialite.idspecialite 
        AND intervenant_has_specialite.idintervenant = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère les informations d'un intervenant donné.
     * @param idintervenant
     * @return Tableau
     */
    function getIntervenant($id){

        global $pdo;

        $query = 'SELECT * FROM intervenant WHERE idintervenant = :id ;';
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Ajoute un établissement à la base de données.
     * @param Tableau $_POST du formulaire intervenants/ajouter.php
     */
    function addIntervenant($params){

        global $pdo;

        $nom = $params['nom'];
        $adresse = $params['adresse'];
        $email = $params['email'];
        $jour = $params['jour'];
        $jour = validerDate($jour);
        $mois = $params['mois'];
        $mois = validerDate($mois);
        $annee = $params['annee'];
        $dateNaissance = "$annee-$mois-$jour";
        
        $query = "INSERT INTO `intervenant` VALUES ('', '$nom', '$adresse', '$email', '$dateNaissance');";
        try{
            $prep = $pdo->prepare($query);
            echo'<br>'.$query.'<br>';
            $prep->execute();
        }
        catch(Exeption $e){
            die("erreur dans la requête ".$e->getMessage());
        }
    }

    /**
     * Supprime un intervenant de la base de données.
     * @param idintervenant
     */
    function deleteIntervenant($params){
        
        global $pdo;
        
        $id = $params['supprimer_intervenant'];
        
        $query = "DELETE FROM intervenant WHERE idintervenant = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
        }
        catch(Exeption $e){
            die("ERREUR : ".$e->getMessage());
        }
    }

    /**
     * Edite un intervenant de la base de données.
     * @param Tableau $_POST du formulaire
     */
    function editIntervenant($params){

        global $pdo;

        $id = $params['id'];
        $nom = $params['nom'];
        $adresse = $params['adresse'];
        $email = $params['email'];
        $dateNaissance = $params['dateNaissance'];
        $dateNaissance = inverserDate($dateNaissance);
        echo $dateNaissance;

        $query = "UPDATE intervenant SET 
        Nom_intervenant='$nom', 
        Adresse_intervenant='$adresse', 
        Email_intervenant='$email', 
        Date_naissance_intervenant='$dateNaissance'
        WHERE idintervenant = $id;";
        try{
            $pdo->query($query);
        }
        catch(Exeption $e){
            die("erreur dans la requête ".$e->getMessage());
        }
    }
}
