<?php
require_once('connect-db.php');

/**Formations*/{

    /**
     * Récupère les infotmations de chaque formations.
     * @return Tableau
     */  
    function getAllFormations(){

        global $pdo;

        $query = "SELECT * FROM formation;";
        try{
            $result = $pdo->query($query)->fetchAll();
            return $result;
        }
        catch (Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    /**
     * récupère touts les modules dans la base de données.
     * @return Tableau
     */
    function getAllModules(){

        global $pdo;

        $query = "SELECT * FROM module;";
        try{
            $result = $pdo->query($query)->fetchAll();
            return $result;
        }
        catch (Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }
    
    /**
     * récupère toutes les séances dans la base de données.
     * @return Tableau
     */
    function getAllSeances(){

        global $pdo;

        $query = "SELECT * FROM seance, type_seance WHERE seance.idtype_seance = type_seance.idtype_seance;";
        try{
            $result = $pdo->query($query)->fetchAll();
            return $result;
        }
        catch (Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère le nom de l'établissement attaché à la formation.
     * @param idformation
     * @return String
     */
    function getEtablissementByFormation($id){

        global $pdo;

        $query = 
        "SELECT * FROM etablissement, formation
        WHERE etablissement.idetablissement = formation.idetablissement 
        AND formation.idformation = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère le nom des modules d'une formation.
     * @param idformation
     * @return Tableau
     */
    function getModulesByFormation($id){

        global $pdo;

        $query = 
        "SELECT * FROM module, formation_has_module
        WHERE module.idmodule = formation_has_module.idmodule 
        AND formation_has_module.idformation = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère le nom de type des seances d'un module.
     * @param idmodule
     * @return Tableau
     */
    function getTypesSeancesByModule($id){

        global $pdo;

        $query = 
        "SELECT type_seance.Nom_type_seance FROM seance, type_seance
        WHERE type_seance.idtype_seance = seance.idtype_seance 
        AND seance.idmodule = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère les informations d'un type de séance donnée.
     * @param idtype_seance
     * @return Tableau
     */
    function getTypeSeance($id){

        global $pdo;

        $query = 
        "SELECT * FROM type_seance WHERE idtype_seance = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère les séances d'un module donné.
     * @param idmodule
     * @return Tableau
     */
    function getSeancesByModule($id){

        global $pdo;

        $query=
        "SELECT seance.idseance, seance.idtype_seance, seance.idintervenant, seance.idmodule 
        FROM seance, module
        WHERE seance.idmodule = module.idmodule
        AND module.idmodule = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die("ERREUR : ".$e->getMessage());
        }
    }

    /**
     * Récupère dans l'ordre chaque date ayant des séances pour un établissement donné. 
     * @param idetablissement
     * @return tableau
     */
    function getDatesBySeancesByEtablissement($id){

        global $pdo;

        $query = "SELECT DISTINCT Date_seance FROM classe_has_seance, classe
        WHERE classe.idetablissement = $id        
        ORDER BY Date_seance;";

        $result = $pdo->query($query);
        return $result->fetchAll();
    }

    /**
     * Récupère les séances d'un établissement donné à une date donnée.
     * @param idetablissement,Date_seance
     * @return tableau
     */
    function getSeancesByEtablissementByDate($idetablissement, $date){

        global $pdo;

        $query = "SELECT classe.Nom_classe, classe_has_seance.Duree_seance, type_seance.Nom_type_seance 
        FROM classe_has_seance, classe, seance, type_seance
        WHERE classe_has_seance.Date_seance = '$date'
        AND classe.idetablissement = $idetablissement
        AND classe_has_seance.idclasse = classe.idclasse
        AND classe_has_seance.idseance = seance.idseance
        AND seance.idtype_seance = type_seance.idtype_seance;";

        $result = $pdo->query($query);
        return $result->fetchAll();
    }

    /** 
     * récupère le nombre d'heures pour une séance donnée.
     * @param idseance
     * @return Duree_seance
     */
    function getHeuresBySeance($id){

        global $pdo;

        $query ="SELECT Duree_seance FROM classe_has_seance WHERE idseance = :id;";
        $prep = $pdo->prepare($query);
        $prep->bindValue(':id', $id);
        $prep->execute();
        return $prep->fetch();
    }

    /** 
     * Récupère toutes les informations des séances des module des formations d'un établissement.
     * @return Tableau
     */
    function getSeancesByModuleByFormationByEtablissement($id){

        global $pdo;

        $query=
        "SELECT seance.idseance, seance.idtype_seance, seance.idintervenant, seance.idmodule 
        FROM seance, module, formation_has_module, formation 
        WHERE seance.idmodule = module.idmodule
        AND module.idmodule = formation_has_module.idmodule
        AND formation_has_module.idformation = formation.idformation
        AND formation.idetablissement = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die("ERREUR : ".$e->getMessage());
        }
    }

    /** 
     * Récupère toutes les informations des module des formations d'un établissement.
     * @return Tableau
     */
    function getModulesByFormationByEtablissement($id){

        global $pdo;

        $query=
        "SELECT module.idmodule, module.Nom_module
        FROM module, formation_has_module, formation 
        WHERE module.idmodule = formation_has_module.idmodule
        AND formation_has_module.idformation = formation.idformation
        AND formation.idetablissement = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die("ERREUR : ".$e->getMessage());
        }
    }

    /**
     * Récupère les informations d'une formation donnée.
     * @param idformation
     * @return Tableau
     */
    function getFormation($id){

        global $pdo;

        $query = 'SELECT * FROM formation WHERE idformation = :id ;';
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

     /**
     * Récupère les informations d'un module donné.
     * @param idmodule
     * @return Tableau
     */
    function getModule($id){

        global $pdo;

        $query = 'SELECT * FROM module WHERE idmodule = :id ;';
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }
    
    /**
     * Ajoute une formation à la base de données.
     * @param Tableau $_POST du formulaire intervenants/ajouter.php
     */ 
    function addFormation($params){

        global $pdo;

        $nom = $params['nom'];
        $idetablissement = $params['etablissement'];
        $nbmodule = $params['nbmodule'];
        //On insère le nom de la formation et l'id de l'établissement affecté.
        $query = "INSERT INTO `formation` VALUES ('', '$nom', '$idetablissement');";
        try{
            $prep = $pdo->prepare($query);
            $prep->execute();
        }
        catch(Exeption $e){
            die("erreur dans la requête ".$e->getMessage());
        }

        global $pdo;
        //On récupère l'id de la formation qui vient d'être ajoutée.
        $query = "SELECT idformation FROM `formation` WHERE Nom_formation = '$nom' AND idetablissement = '$idetablissement';";
        try{
            $prep = $pdo->prepare($query);
            $prep->execute();
            $idformation = $prep->fetch()->idformation;
        }
        catch(Exeption $e){
            die("erreur dans la requête ".$e->getMessage());
        }

        global $pdo;
        //On affecte tous les modules souhaités à la formation dont on viens de récupérer l'id.
        for($i=1 ; $i<$nbmodule+1 ; $i++){
            if(isset($params['module'.$i])){
                $idmodule = $params['module'.$i];
                echo($idmodule);
                echo("########");
                $query = "INSERT INTO `formation_has_module` (`idformation`, `idmodule`) VALUES ('$idformation', '$idmodule');";
                try{
                    $prep = $pdo->prepare($query);
                    $prep->execute();
                }
                catch(Exeption $e){
                    die("erreur dans la requête ".$e->getMessage());
                }
            }
        }




    }
    
    /**
     * Supprime une formation de la base de données.
     * @param idformation
     */
    function deleteFormation($params){
        
        global $pdo;
        
        $id = $params['supprimer_formation'];
        
        $query =    
        "DELETE FROM formation WHERE idformation = :id;
        DELETE FROM formation_has_module WHERE idformation = :id";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
        }
        catch(Exeption $e){
            die("ERREUR : ".$e->getMessage());
        }
    }

    /** 
     * Modifie une formation dans la base de données.
     * @param Tableau
     */
    function editFormation($params){

        global $pdo;

        $id = $params['id'];
        $nom = $params['nom'];
        $idetablissement = $params['etablissement'];
        

        $query = "UPDATE formation SET 
        Nom_formation='$nom',  
        idetablissement='$idetablissement' 
        WHERE idformation = '$id';";
        try{
            $pdo->query($query);
        }
        catch(Exeption $e){
            die("erreur dans la requête ".$e->getMessage());
        }
    }
}
