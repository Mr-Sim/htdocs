<?php
require_once('connect-db.php');

/**Opérations*/{

    /**
     * Valide les numéros de la date : 
     * Ajoute un zéro devant le premier caractère si la variable en paramètre ne contient qu'un caractère.
     * @param varchar (1, 2)
     * @return varchar (2)
     */
    function validerDate($param){
        $date = $param;
        if (strlen($date) == 1)
            $date="0$date";
        return $date;
    }

    /**
     * Verifie si une date est au format demandé.
     * Fonction utilisée par la fonction inverserDate().
     * @param date
     * @param format
     * @return boolean
     */
    function isValid($date, $format){
        $dt = DateTime::createFromFormat($format, $date);
        return $dt && $dt->format($format) === $date;
    }

    /**
     * Inverse la date et change les caractères de séparation pour convertir une date au format français ou américain.
     * @param date format 1 ou format 2
     * @return date format 2 ou format 1
     */
    function inverserDate($param){
        if(isValid($param, $format = 'Y-m-d')){
            $date = str_replace('-', '/', $param);
            return date('d/m/Y', strtotime($date));
        }
        elseif(isValid($param, $format = 'd/m/Y')){
            $date = str_replace('/', '-', $param);
            return date('Y-m-d', strtotime($date));
        }
    }



}





























