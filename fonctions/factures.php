<?php
require_once('connect-db.php');

/**Factures*/{

    /**
     * Récupère les infotmations de chaque factures.
     * @return Tableau
     */  
    function getAllFactures(){

        global $pdo;

        $query = "SELECT * FROM facture ORDER BY idfacture;";
        try{
            $result = $pdo->query($query)->fetchAll();
            return $result;
        }
        catch (Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère les séances prises en compte par une facture.
     * @param idfacture
     * @return Tableau
     */
    function getSeancesByFacture($id){
        /**
        global $pdo;

        $query = 
        "SELECT * FROM seance, intervenant_has_diplome
        WHERE diplome.iddiplome = intervenant_has_diplome.iddiplome 
        AND intervenant_has_diplome.idintervenant = :id;";
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die("Erreur : ".$e->getMessage());
        }
    */}

    /**
     * Récupère les informations d'une facture donnée.
     * @param idfacture
     * @return Tableau
     */
    function getFacture($id){

        global $pdo;

        $query = 'SELECT * FROM facture WHERE idfacture = :id ;';
        try{
            $prep = $pdo->prepare($query);
            $prep->bindValue(':id', $id);
            $prep->execute();
            $result = $prep->fetchAll();
            return $result;
        }
        catch(Exeption $e){
            die ("Erreur : ".$e->getMessage());
        }
    }

    /**
     * Récupère les informations de la dernière facture enregistrée.
     */
    function getLastFacture(){
    
        global $pdo;
    
        $query = "SELECT * FROM facture WHERE idfacture=(SELECT max(idfacture) FROM facture);";
        try{
            $result = $pdo->query($query)->fetch();
            return $result;
        }
        catch(Exeption $e){
            die("Erreur dans la requête : ".$e->getMessage());
        }
    }

    /**
     * Récupère le taux horaire d'une séance donnée.
     * @param idseance
     * @return int
     */
    function getTauxHorairesBySeance($id){

        global $pdo;

        $query = "SELECT Taux_horaire FROM type_seance, seance WHERE type_seance.idtype_seance = seance.idtype_seance AND seance.idseance = $id;";
        $result = $pdo->query($query);
        return $result->fetch();
    }

    /**
     * Calcule le montant de la facture d'un établissement.
     * @param idetablissement
     * @return int 
     */
    function setMontant($idetablissement){

        //On récupère chaque modules de chaque formations pour l'établissement choisi.
        $modules = getModulesByFormationByEtablissement($idetablissement);
        $idtype_seance=0;
        $total = 0;

        foreach($modules as $module){
            //On récupère l'id du module
            $idmodule = $module->idmodule;
            //On récupère toutes les séances de ce module
            $seances = getSeancesByModule($idmodule);
            //Pour chaque séances, on récupère son taux horaire et le nombre d'heures.
            foreach($seances as $seance){
                $idseance = $seance->idseance;
                $h=getHeuresBySeance($idseance);
                $h = $h->Duree_seance;
                $prix=getTauxHorairesBySeance($idseance);
                $prix = $prix->Taux_horaire;
                $total += ($prix*$h);
            }
        }
        return $total;
    }


    /**
     * Complète les informations de la dernière facture enregistrée dans la base de données.
     * Complète le Numéro de facture, et les lignes de factures.
     */
    function completeLastFacture(){

        global $pdo;

        //Génération du numéro de facture
        $lastFacture = getLastFacture();
        foreach($lastFacture as $param){
            $idfacture=$param->idfacture;
            $idfacture=validerDate($idfacture);

            $idetablissement=$param->idetablissement;
            $montant = setMontant($idetablissement);
            $mois=$param->Mois;
            $mois= validerDate($mois);
            $annee=$param->Annee;
            $numeroFacture=$mois.$annee."-".$idfacture;
        }
        //Génération des lignes de factures
        /**
         * Non fonctionnel.
         * Cause : Je ne sais pas comment sotcker le stdClass retourné par la fonction
         * Solutions possibles : convertir le stdClass en String. J'ai pas réussi.
         * 
         * require_once("formations.php");
         * $array = getSeancesByModuleByFormationByEtablissement($idetablissement);
         * $seances = implode("||", $array);
         * echo($seances);
         *
         * $query = "UPDATE facture SET Numero_facture='$numeroFacture', Ligne_facture='$seances' WHERE idfacture='$idfacture';";
         */
        $query = "UPDATE facture SET Numero_facture='$numeroFacture', Montant_facture='$montant' WHERE idfacture='$idfacture';";
        try{
            $pdo->query($query);
        }
         catch(Exeption $e){
            die("ERREUR : ".$e->getMessage());
        }
         
    }


    /**
     * Complète les informations de la facture donnée.
     * Complète le Numéro de facture, le montant de facture et les lignes de facture.
     */
    function completeFacture($idfacture){

        global $pdo;

        //Génération du numéro de facture
        $facture = getFacture($idfacture);
        foreach($facture as $param){
            $idfacture=validerDate($idfacture);

            $idetablissement=$param->idetablissement;
            $montant = setMontant($idetablissement);
            $mois=$param->Mois;
            $mois= validerDate($mois);
            $annee=$param->Annee;
            $numeroFacture=$mois.$annee."-".$idfacture;
        }
        //Génération des lignes de factures
        /**
         * Non fonctionnel.
         * Cause : Je ne sais pas comment sotcker le stdClass retourné par la fonction
         * Solutions possibles : convertir le stdClass en String. J'ai pas réussi.
         * 
         * require_once("formations.php");
         * $array = getSeancesByModuleByFormationByEtablissement($idetablissement);
         * $seances = implode("||", $array);
         * echo($seances);
         *
         * $query = "UPDATE facture SET Numero_facture='$numeroFacture', Ligne_facture='$seances' WHERE idfacture='$idfacture';";
         */
        $query = "UPDATE facture SET Numero_facture='$numeroFacture', Montant_facture='$montant' WHERE idfacture='$idfacture';";
        try{
            $pdo->query($query);
        }
         catch(Exeption $e){
            die("ERREUR : ".$e->getMessage());
        }
         
    }

   
    /**
     * Ajoute une facture à la base de données.
     * @param Tableau $_POST du formulaire /factures/ajouter.php
     */
    function addFacture($params){

        global $pdo;

        $jour = $params['jour'];
        $jour = validerDate($jour);
        $mois = $params['mois'];
        $mois = validerDate($mois);
        $annee = $params['annee'];
        $dateFacture = "$annee-$mois-$jour";

        $idetablissement = $params['idetablissement'];
        $phase = $params['phase'];
        
        $query = "INSERT INTO facture VALUES ('', NULL, '$dateFacture', '', NULL, '$mois', '$annee', '$phase', '$idetablissement');";
        try{
            $prep = $pdo->prepare($query);
            echo'<br>'.$query.'<br>';
            $prep->execute();
            completeLastFacture();
        }
        catch(Exeption $e){
            die("erreur dans la requête ".$e->getMessage());
        }
    }


    /**
     * Modifie une facture dans la base de données.
     * @param Tableau $_POST du formulaire /factures/modifier.php
     */
    function editFacture($params){

        global $pdo;

        $id = $params['id'];
        $dateFacture = $params['date'];
        $dateFacture = inverserDate($dateFacture);
        $phase = $params['phase'];
        $etablissement = $params['etablissement'];
        
        echo $dateFacture;

        $query = "UPDATE facture SET 
        Date_facture='$dateFacture', 
        Phase='$phase', 
        idetablissement='$etablissement'
        WHERE idfacture = $id;";
        try{
            $pdo->query($query);
            completeFacture($id);
        }
        catch(Exeption $e){
            die("erreur dans la requête ".$e->getMessage());
        }
        
    }
}
