<?php
require_once '../header.php';
require_once '../fonctions/etablissements.php';

//On récupère les informations de chaque établissements dans la base de données
$listeEtablissements = getAllEtablissements();
?>

Liste des établissements

<div>

    
    <form action=ajouter.php>
        <button type="submit" action="ajouter.php" class="btn btn-primary">Ajouter un établissement</button>
    </form>

<div>
    <table class="table-bordered">
        
        <th>ID</th>
        <th>Etablissement</th>
        <th>Type</th>
        <th>Adresse</th>
        <th>Site Web</th>
        <th>Téléphone</th>
        <th>Email</th>
        <th>Interlocuteur</th>
    
        <?php //On insère chaque informations dans le tableau avec un foreach. 
        foreach($listeEtablissements as $item): ?>
        <tr>
            <td><?php echo $item->idetablissement;?></td>
            <td><?php echo $item->Nom_etablissement;?><br>
                <p1><a href='modifier.php?edit=<?php echo $item->idetablissement;?>'>MODIFIER</a>
                <a href='../model.php?supprimer_etablissement=<?php echo $item->idetablissement;?>' onClick="return(confirm('Etes-vous sûr de vouloir supprimer <?php echo $item->Nom_etablissement;?> ?'));">SUPPRIMER</a></td>
            <td><?php echo $item->Type_etablissement;?></td>
            <td><?php echo $item->Adresse_etablissement;?></td>
            <td><?php echo $item->Site_web_etablissement;?></td>
            <td><?php echo $item->Telephone_etablissement;?></td>
            <td><?php echo $item->Email_etablissement;?></td>
            <td><?php echo $item->Interlocuteur_etablissement;?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>




<?php
require_once '../footer.php';
?>

