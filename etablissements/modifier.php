<?php
require_once '../header.php';
require_once('../fonctions/etablissements.php');

//On récupère l'id de l'établissement dans l'URL
$id=$_GET['edit'];
print $id;
//On répupère les informations de l'établissement à partir de son id.
$etablissement = getEtablissement($id);
print_r($etablissement);
//On affecte chaque information à une variable.


foreach ($etablissement as $param):
    $nom = $param->Nom_etablissement;
    $adresse = $param->Adresse_etablissement;
    $site_web = $param->Site_web_etablissement;
    $telephone = $param->Telephone_etablissement;
    $email = $param->Email_etablissement;
    $interlocuteur = $param->Interlocuteur_etablissement;
    $type = $param->Type_etablissement;
endforeach;
?>

Modifier un etablissement

<div>
    <form action='../model.php' method=post>
    ID Etablissement : <input readonly=readonly type=text name="id" value="<?php echo $id; ?>"><br>
    Nom Etablissement : <input type=text name="nom" value="<?php echo $nom; ?>"><br>
    Adresse : <input type=text name="adresse" value="<?php echo $adresse; ?>"><br>
    Site Web : <input type=text name="site_web" value="<?php echo $site_web; ?>"><br>
    Telephone : <input type=text name="telephone" value="<?php echo $telephone; ?>"><br>
    Email : <input type=text name="email" value="<?php echo $email; ?>"><br>
    Interlocuteur : <input type=text name="interlocuteur" value="<?php echo $interlocuteur; ?>"><br>
    Type d'établissement :  <input type=radio name="type" value="publique" <?php if($type=="publique") echo"checked" ?>> Publique<br>
                            <input type=radio name="type" value="privé"<?php if($type=="privé") echo"checked" ?>> Privé<br>
    <input type=submit name="modifier_etablissement" value="Enregistrer les modifications"><br>
    </form>
</div>




<?php
require_once '../footer.php';
?>
