-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : dim. 27 juin 2021 à 18:27
-- Version du serveur :  10.4.19-MariaDB
-- Version de PHP : 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `enslar`
--

-- --------------------------------------------------------

--
-- Structure de la table `classe`
--

CREATE TABLE `classe` (
  `idclasse` int(11) NOT NULL,
  `Nom_classe` varchar(20) NOT NULL,
  `idetablissement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `classe`
--

INSERT INTO `classe` (`idclasse`, `Nom_classe`, `idetablissement`) VALUES
(1, 'SIO11', 1),
(2, 'SIO12', 1);

-- --------------------------------------------------------

--
-- Structure de la table `classe_has_seance`
--

CREATE TABLE `classe_has_seance` (
  `idclasse` int(11) NOT NULL,
  `idseance` int(11) NOT NULL,
  `Numero_salle` varchar(10) NOT NULL,
  `Effectif_present` int(11) DEFAULT 0,
  `Effectif_absent` int(11) DEFAULT 0,
  `Date_seance` date NOT NULL,
  `Duree_seance` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `classe_has_seance`
--

INSERT INTO `classe_has_seance` (`idclasse`, `idseance`, `Numero_salle`, `Effectif_present`, `Effectif_absent`, `Date_seance`, `Duree_seance`) VALUES
(1, 1, 'C420', 15, 7, '2021-06-02', 3),
(2, 2, 'C417', 30, 2, '2021-05-13', 1),
(1, 3, 'C420', 0, 0, '2021-06-14', 3),
(2, 4, 'C417', 0, 0, '2021-06-10', 3),
(1, 5, 'C420', 0, 0, '2021-06-25', 3),
(2, 6, 'C417', 0, 0, '2021-05-13', 1),
(1, 7, 'ho', 0, 0, '2021-06-08', 5),
(1, 8, 'c450', 0, 0, '2021-06-25', 4),
(2, 9, '545', 0, 0, '2021-06-10', 1),
(1, 10, '159', 0, 0, '2021-06-16', 959),
(1, 11, 'C5', 0, 0, '2021-06-08', 486),
(1, 12, '15B', 0, 0, '2021-06-29', 159);

-- --------------------------------------------------------

--
-- Structure de la table `diplome`
--

CREATE TABLE `diplome` (
  `iddiplome` int(11) NOT NULL,
  `Nom_diplome` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `diplome`
--

INSERT INTO `diplome` (`iddiplome`, `Nom_diplome`) VALUES
(1, 'BAC'),
(2, 'Doctorat');

-- --------------------------------------------------------

--
-- Structure de la table `etablissement`
--

CREATE TABLE `etablissement` (
  `idetablissement` int(11) NOT NULL,
  `Nom_etablissement` varchar(50) NOT NULL,
  `Type_etablissement` varchar(45) NOT NULL,
  `Adresse_etablissement` varchar(90) NOT NULL,
  `Site_web_etablissement` varchar(90) NOT NULL,
  `Telephone_etablissement` varchar(20) NOT NULL,
  `Email_etablissement` varchar(90) NOT NULL,
  `Interlocuteur_etablissement` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `etablissement`
--

INSERT INTO `etablissement` (`idetablissement`, `Nom_etablissement`, `Type_etablissement`, `Adresse_etablissement`, `Site_web_etablissement`, `Telephone_etablissement`, `Email_etablissement`, `Interlocuteur_etablissement`) VALUES
(0, 'Indépendant/Autre', '', '', '', '', '', ''),
(1, 'Lycée Léonnard De Vinci', 'publique', '13 rue mystère, Melun', 'http://www2.vinci-melun.org/', '06 60 61 62 63', 'lycee@vinci.com', 'Monsieur Directeur'),
(4, 'Lycée Toto', 'privé', '14 rue de la Rigolade', 'toto.ecoledurire.haha', '0913692758', 'ecoledurire@toto.com', 'Toto en personne !'),
(5, 'Etablissement lambda', 'privé', '27 avenue personnage lambda', 'www.lambda.ac-creteil.com', '01 21 21 21 21', 'etablissement.lambda@mail.com', 'Être lambda');

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

CREATE TABLE `facture` (
  `Numero_facture` varchar(45) NOT NULL,
  `idfacture` int(11) NOT NULL,
  `Date_facture` date NOT NULL,
  `Montant_facture` double NOT NULL,
  `Ligne_facture` longtext DEFAULT NULL,
  `Mois` int(11) NOT NULL,
  `Annee` int(11) NOT NULL,
  `Phase` varchar(45) NOT NULL,
  `idetablissement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `facture`
--

INSERT INTO `facture` (`Numero_facture`, `idfacture`, `Date_facture`, `Montant_facture`, `Ligne_facture`, `Mois`, `Annee`, `Phase`, `idetablissement`) VALUES
('062021-01', 1, '2021-06-16', 1000000.5, 'Wow wow wow c\'est qu\'un exemple je peux pas ecrire tout ça à la main', 6, 2021, 'En attente', 1),
('062021-37', 37, '2021-06-22', 159, NULL, 6, 2021, 'En attente', 1),
('072015-36', 36, '2015-07-13', 100, NULL, 7, 2015, 'Envoyée', 4);

-- --------------------------------------------------------

--
-- Structure de la table `formation`
--

CREATE TABLE `formation` (
  `idformation` int(11) NOT NULL,
  `Nom_formation` varchar(45) NOT NULL,
  `idetablissement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `formation`
--

INSERT INTO `formation` (`idformation`, `Nom_formation`, `idetablissement`) VALUES
(1, 'Réseau', 1),
(2, 'Packet tracerer', 0),
(28, 'LODA IL SOLE', 4),
(29, 'TEST', 0);

-- --------------------------------------------------------

--
-- Structure de la table `formation_has_module`
--

CREATE TABLE `formation_has_module` (
  `idformation` int(11) NOT NULL,
  `idmodule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `formation_has_module`
--

INSERT INTO `formation_has_module` (`idformation`, `idmodule`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(2, 3),
(2, 4),
(28, 1),
(28, 3),
(28, 4),
(29, 1),
(29, 2),
(29, 3),
(29, 4);

-- --------------------------------------------------------

--
-- Structure de la table `intervenant`
--

CREATE TABLE `intervenant` (
  `idintervenant` int(11) NOT NULL,
  `Nom_intervenant` varchar(50) NOT NULL,
  `Adresse_intervenant` varchar(90) NOT NULL,
  `Email_intervenant` varchar(90) NOT NULL,
  `Date_naissance_intervenant` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `intervenant`
--

INSERT INTO `intervenant` (`idintervenant`, `Nom_intervenant`, `Adresse_intervenant`, `Email_intervenant`, `Date_naissance_intervenant`) VALUES
(1, 'Guilien ', '14 rue Mystère, Melun', 'Guilien@vinci.com', '2036-06-02'),
(2, 'Guyon', '14bis rue Mystère, Melun', 'Guyon@vinci.com', '2021-06-02'),
(8, 'Patrick Sébastien', 'Moulin rouge, Paris', 'patrick.sebastien@orange.fr', '1935-05-19');

-- --------------------------------------------------------

--
-- Structure de la table `intervenant_has_diplome`
--

CREATE TABLE `intervenant_has_diplome` (
  `idintervenant` int(11) NOT NULL,
  `iddiplome` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `intervenant_has_diplome`
--

INSERT INTO `intervenant_has_diplome` (`idintervenant`, `iddiplome`) VALUES
(1, 1),
(1, 2),
(2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `intervenant_has_specialite`
--

CREATE TABLE `intervenant_has_specialite` (
  `idintervenant` int(11) NOT NULL,
  `idspecialite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `intervenant_has_specialite`
--

INSERT INTO `intervenant_has_specialite` (`idintervenant`, `idspecialite`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `module`
--

CREATE TABLE `module` (
  `idmodule` int(11) NOT NULL,
  `Nom_module` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `module`
--

INSERT INTO `module` (`idmodule`, `Nom_module`) VALUES
(1, 'Adressage réseau'),
(2, 'Packet tracer'),
(3, 'PHP'),
(4, 'JAVA');

-- --------------------------------------------------------

--
-- Structure de la table `seance`
--

CREATE TABLE `seance` (
  `idseance` int(11) NOT NULL,
  `idintervenant` int(11) NOT NULL,
  `idtype_seance` int(11) NOT NULL,
  `idmodule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `seance`
--

INSERT INTO `seance` (`idseance`, `idintervenant`, `idtype_seance`, `idmodule`) VALUES
(1, 1, 2, 1),
(2, 2, 4, 3),
(3, 1, 2, 2),
(4, 2, 5, 3),
(5, 2, 4, 4),
(6, 2, 5, 2),
(7, 8, 3, 3),
(8, 8, 4, 3),
(9, 8, 1, 3),
(10, 8, 2, 3),
(11, 1, 2, 1),
(12, 1, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `specialite`
--

CREATE TABLE `specialite` (
  `idspecialite` int(11) NOT NULL,
  `Nom_specialite` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `specialite`
--

INSERT INTO `specialite` (`idspecialite`, `Nom_specialite`) VALUES
(1, 'réseau'),
(2, 'dévellopement');

-- --------------------------------------------------------

--
-- Structure de la table `test`
--

CREATE TABLE `test` (
  `test` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `test`
--

INSERT INTO `test` (`test`) VALUES
('Connexion avec la base de donnée fonctionnelle '),
('haha');

-- --------------------------------------------------------

--
-- Structure de la table `type_seance`
--

CREATE TABLE `type_seance` (
  `idtype_seance` int(11) NOT NULL,
  `Nom_type_seance` varchar(45) NOT NULL,
  `Taux_horaire` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `type_seance`
--

INSERT INTO `type_seance` (`idtype_seance`, `Nom_type_seance`, `Taux_horaire`) VALUES
(1, 'TP', '30'),
(2, 'TD', '35'),
(3, 'Cours', '40'),
(4, 'Projet', '45'),
(5, 'Evaluation', '45');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idutilisateur` int(11) NOT NULL,
  `Nom_utilisateur` varchar(45) NOT NULL,
  `Prenom_utilisateur` varchar(45) NOT NULL,
  `Login_utilisateur` varchar(45) NOT NULL,
  `Password_utilisateur` varchar(45) NOT NULL,
  `Telephone_utilisateur` varchar(45) NOT NULL,
  `Email_utilisateur` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idutilisateur`, `Nom_utilisateur`, `Prenom_utilisateur`, `Login_utilisateur`, `Password_utilisateur`, `Telephone_utilisateur`, `Email_utilisateur`) VALUES
(1, 'Simon', 'Catanese', 'root', 'root', '0768056862', 'simoncatanese.pro@gmail.com');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `classe`
--
ALTER TABLE `classe`
  ADD PRIMARY KEY (`idclasse`),
  ADD KEY `idetablissement` (`idetablissement`);

--
-- Index pour la table `classe_has_seance`
--
ALTER TABLE `classe_has_seance`
  ADD KEY `idclasse` (`idclasse`,`idseance`),
  ADD KEY `classe_has_seance_ibfk_2` (`idseance`);

--
-- Index pour la table `diplome`
--
ALTER TABLE `diplome`
  ADD PRIMARY KEY (`iddiplome`);

--
-- Index pour la table `etablissement`
--
ALTER TABLE `etablissement`
  ADD PRIMARY KEY (`idetablissement`);

--
-- Index pour la table `facture`
--
ALTER TABLE `facture`
  ADD PRIMARY KEY (`Numero_facture`),
  ADD UNIQUE KEY `idfacture` (`idfacture`),
  ADD KEY `idetablissement` (`idetablissement`);

--
-- Index pour la table `formation`
--
ALTER TABLE `formation`
  ADD PRIMARY KEY (`idformation`),
  ADD KEY `idetablissement` (`idetablissement`);

--
-- Index pour la table `formation_has_module`
--
ALTER TABLE `formation_has_module`
  ADD KEY `idformation` (`idformation`,`idmodule`),
  ADD KEY `idmodule` (`idmodule`);

--
-- Index pour la table `intervenant`
--
ALTER TABLE `intervenant`
  ADD PRIMARY KEY (`idintervenant`);

--
-- Index pour la table `intervenant_has_diplome`
--
ALTER TABLE `intervenant_has_diplome`
  ADD KEY `idintervenant` (`idintervenant`,`iddiplome`),
  ADD KEY `iddiplome` (`iddiplome`);

--
-- Index pour la table `intervenant_has_specialite`
--
ALTER TABLE `intervenant_has_specialite`
  ADD KEY `idintervenant` (`idintervenant`,`idspecialite`),
  ADD KEY `idspecialisation` (`idspecialite`);

--
-- Index pour la table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`idmodule`);

--
-- Index pour la table `seance`
--
ALTER TABLE `seance`
  ADD PRIMARY KEY (`idseance`),
  ADD KEY `idintervenant` (`idintervenant`),
  ADD KEY `idtype_seance` (`idtype_seance`,`idmodule`),
  ADD KEY `idmodule` (`idmodule`);

--
-- Index pour la table `specialite`
--
ALTER TABLE `specialite`
  ADD PRIMARY KEY (`idspecialite`);

--
-- Index pour la table `type_seance`
--
ALTER TABLE `type_seance`
  ADD PRIMARY KEY (`idtype_seance`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`idutilisateur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `classe`
--
ALTER TABLE `classe`
  MODIFY `idclasse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `diplome`
--
ALTER TABLE `diplome`
  MODIFY `iddiplome` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `etablissement`
--
ALTER TABLE `etablissement`
  MODIFY `idetablissement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `facture`
--
ALTER TABLE `facture`
  MODIFY `idfacture` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT pour la table `formation`
--
ALTER TABLE `formation`
  MODIFY `idformation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT pour la table `intervenant`
--
ALTER TABLE `intervenant`
  MODIFY `idintervenant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `module`
--
ALTER TABLE `module`
  MODIFY `idmodule` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `seance`
--
ALTER TABLE `seance`
  MODIFY `idseance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `specialite`
--
ALTER TABLE `specialite`
  MODIFY `idspecialite` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `type_seance`
--
ALTER TABLE `type_seance`
  MODIFY `idtype_seance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `idutilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `classe`
--
ALTER TABLE `classe`
  ADD CONSTRAINT `classe_ibfk_1` FOREIGN KEY (`idetablissement`) REFERENCES `etablissement` (`idetablissement`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `classe_has_seance`
--
ALTER TABLE `classe_has_seance`
  ADD CONSTRAINT `classe_has_seance_ibfk_1` FOREIGN KEY (`idclasse`) REFERENCES `classe` (`idclasse`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `classe_has_seance_ibfk_2` FOREIGN KEY (`idseance`) REFERENCES `seance` (`idseance`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `facture`
--
ALTER TABLE `facture`
  ADD CONSTRAINT `facture_ibfk_1` FOREIGN KEY (`idetablissement`) REFERENCES `etablissement` (`idetablissement`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `formation`
--
ALTER TABLE `formation`
  ADD CONSTRAINT `formation_ibfk_1` FOREIGN KEY (`idetablissement`) REFERENCES `etablissement` (`idetablissement`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `formation_has_module`
--
ALTER TABLE `formation_has_module`
  ADD CONSTRAINT `formation_has_module_ibfk_1` FOREIGN KEY (`idformation`) REFERENCES `formation` (`idformation`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `formation_has_module_ibfk_2` FOREIGN KEY (`idmodule`) REFERENCES `module` (`idmodule`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `intervenant_has_diplome`
--
ALTER TABLE `intervenant_has_diplome`
  ADD CONSTRAINT `intervenant_has_diplome_ibfk_1` FOREIGN KEY (`idintervenant`) REFERENCES `intervenant` (`idintervenant`),
  ADD CONSTRAINT `intervenant_has_diplome_ibfk_2` FOREIGN KEY (`iddiplome`) REFERENCES `diplome` (`iddiplome`);

--
-- Contraintes pour la table `intervenant_has_specialite`
--
ALTER TABLE `intervenant_has_specialite`
  ADD CONSTRAINT `intervenant_has_specialite_ibfk_1` FOREIGN KEY (`idintervenant`) REFERENCES `intervenant` (`idintervenant`),
  ADD CONSTRAINT `intervenant_has_specialite_ibfk_2` FOREIGN KEY (`idspecialite`) REFERENCES `specialite` (`idspecialite`);

--
-- Contraintes pour la table `seance`
--
ALTER TABLE `seance`
  ADD CONSTRAINT `seance_ibfk_2` FOREIGN KEY (`idintervenant`) REFERENCES `intervenant` (`idintervenant`),
  ADD CONSTRAINT `seance_ibfk_3` FOREIGN KEY (`idmodule`) REFERENCES `module` (`idmodule`),
  ADD CONSTRAINT `seance_ibfk_4` FOREIGN KEY (`idtype_seance`) REFERENCES `type_seance` (`idtype_seance`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
